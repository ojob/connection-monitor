from pathlib import Path

import pytest

from connection_monitor.domain import core, model

SECRETS_FFN = Path(__file__).parent.parent / "secrets.yml"
SECRETS = model.RouterSecrets.from_yaml(SECRETS_FFN)


@pytest.mark.io
def test_init():
    stater = core.get_stats_in_period(secrets=SECRETS)
    _, stats0 = next(stater)

    assert isinstance(stats0, model.NetworkStats)
    assert stats0.latency_ms > 0 or stats0.latency_ms == -1  # when get fails
    assert stats0.bytes_out > 0
    assert stats0.bytes_in > 0

    _, stats1 = next(stater)
    assert stats1.latency_ms > 0
    assert stats1.router_stats.mode
