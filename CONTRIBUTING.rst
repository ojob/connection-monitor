CONTRIBUTING
============

1. Clone `the repo`_ and ``cd`` in created folder;
2. Create a ``virtualenv``, activate it, and ``python -m pip install --upgrade pip``
3. Install the dev dependencies: ``pip install -r dev-requirement.txt``
4. Install the package: ``pip install -e .``

.. _the repo: https://framagit.org/ojob/connection-monitor
