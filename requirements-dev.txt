# development tools
python-language-server[all]

# packaging tools
black
setuptools
twine
wheel

# testing tools
pytest
pytest-cov
pytest-mypy
tox

# typing stubs
types-requests
types-PyYAML
