# coding: utf-8
"""Configuration for python package managers."""

# ---- built-in import ----------------------------------------------------
import pathlib

# ---- third-party imports ------------------------------------------------
import setuptools

# ---- constants definition -----------------------------------------------
PKG_PATH = "connection_monitor"
PKG_NAME = PKG_PATH.replace("_", "-")

HERE = pathlib.Path(__file__).parent
VERSION_FN = HERE / "src" / PKG_PATH / "VERSION"
README_FN = pathlib.Path("README.rst")


# ---- setup configuration ------------------------------------------------
setuptools.setup(
    name=PKG_NAME,
    version=VERSION_FN.open().read().strip(),
    author="Joël Bourgault",
    author_email="joel.bourgault@gmail.com",
    description="Connection monitoring utility",
    long_description=README_FN.open().read(),
    long_description_content_type="text/markdown",
    url=f"https://framagit.org/ojob/{PKG_NAME}",
    packages=setuptools.find_packages("src"),
    package_dir={"": "src"},
    classifiers=[
        "Programming Language :: Python :: 3",
        (
            "License :: OSI Approved :: GNU General Public License v3 or later "
            "(GPLv3+)"
        ),
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.7",
    install_requires=[
        "bs4",
        "click",
        "psutil",
        "pyyaml",
        "requests",
    ],
    # define package data in MANIFEST.in
    include_package_data=True,
    # test_suite='districounts.tests',
    entry_points=dict(
        console_scripts=[
            f"monitor={PKG_PATH}.cli:cli",
        ],
    ),
)
